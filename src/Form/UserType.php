<?php

// namespace App\Form;

// use App\Entity\User;
// use Symfony\Component\Form\AbstractType;
// use Symfony\Component\Form\FormBuilderInterface;
// use Symfony\Component\OptionsResolver\OptionsResolver;

// class UserType extends AbstractType
// {
//     public function buildForm(FormBuilderInterface $builder, array $options): void
//     {
//         $builder
//             ->add('email')
//             ->add('roles', ChoiceType::class, [
//                     'choices' => [
//                     'User' => 'ROLE_USER',
//                     'Admin' => 'ROLE_ADMIN',
//                     'Employee' => 'ROLE_EMPLOYEE',
//                     // Add other roles as needed
//                     ],
//                 'multiple' => true, // Allow selecting multiple roles
//                 'expanded' => true, // Display checkboxes for each role
//             ])

//             // ->add('email')
//             ->add('roles')
//             ->add('password')
//             ->add('isVerified')
//             ->add('last_name')
//             ->add('first_name')
//             ->add('registration_date')
//             ->add('street')
//             ->add('postal_code')
//             ->add('city')
//             ->add('phone_number')
//             ->add('registration_number')
//             ->add('status_user')
//             ->add('employe')
//             ->add('cart')
//             ->add('chatSessions')
//         ;
//     }

//     public function configureOptions(OptionsResolver $resolver): void
//     {
//         $resolver->setDefaults([
//             'data_class' => User::class,
//         ]);
//     }
// }


namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType; // Ajout de cette ligne
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'User' => 'ROLE_USER',
                    'Admin' => 'ROLE_ADMIN',
                    'Employee' => 'ROLE_EMPLOYEE',
                    // Add other roles as needed
                ],
                'multiple' => true, // Allow selecting multiple roles
                'expanded' => true, // Display checkboxes for each role
            ])
            // La ligne suivante a été supprimée pour éviter la duplication
            ->add('password')
            ->add('isVerified')
            ->add('last_name')
            ->add('first_name')
            // ->add('registration_date')
            ->add('street')
            ->add('postal_code')
            ->add('city')
            ->add('phone_number')
            ->add('registration_number')
            ->add('status_user')
            // ->add('employe')
            // ->add('cart')
            // ->add('chatSessions')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
