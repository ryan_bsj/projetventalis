<?php

// namespace App\Controller;

// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\Routing\Annotation\Route;

// class ViewProductController extends AbstractController
// {
//     #[Route('/view/product', name: 'app_view_product')]
//     public function index(): Response
//     {
//         return $this->render('view_product/index.html.twig', [
//             'controller_name' => 'ViewProductController',
//         ]);
//     }
// }


namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewProductController extends AbstractController
{
    #[Route('/view/product', name: 'app_view_product')]
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('view_product/index.html.twig', [
            'controller_name' => 'ViewProductController',
            'products' => $productRepository->findAll(),


        ]);
    }
}

