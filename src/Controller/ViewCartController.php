<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewCartController extends AbstractController
{
    #[Route('/view/cart', name: 'app_view_cart')]
    public function index(): Response
    {
        return $this->render('view_cart/index.html.twig', [
            'controller_name' => 'ViewCartController',
        ]);
    }
}
