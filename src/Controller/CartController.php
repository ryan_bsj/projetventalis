<?php

// namespace App\Controller;

// use App\Entity\Cart;
// use App\Form\CartType;
// use App\Repository\CartRepository;
// use Doctrine\ORM\EntityManagerInterface;
// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\Routing\Annotation\Route;

// #[Route('/cart')]
// class CartController extends AbstractController
// {
//     #[Route('/', name: 'app_cart_index', methods: ['GET'])]
//     public function index(CartRepository $cartRepository): Response
//     {
//         return $this->render('cart/index.html.twig', [
//             'carts' => $cartRepository->findAll(),
//         ]);
//     }

//     #[Route('/new/{id}', name: 'app_cart_index', methods: ['GET', 'POST'])]
//     public function new(Request $request, EntityManagerInterface $entityManager): Response
//     {
//         $cart = new Cart();
//         $form = $this->createForm(CartType::class, $cart);
//         $form->handleRequest($request);

//         if ($form->isSubmitted() && $form->isValid()) {
//             $entityManager->persist($cart);
//             $entityManager->flush();

//             return $this->redirectToRoute('app_cart_index', [], Response::HTTP_SEE_OTHER);
//         }

//         return $this->renderForm('cart/new.html.twig', [
//             'cart' => $cart,
//             'form' => $form,
//         ]);
//     }

//     #[Route('/{id}', name: 'app_cart_show', methods: ['GET'])]
//     public function show(Cart $cart): Response
//     {
//         return $this->render('cart/show.html.twig', [
//             'cart' => $cart,
//         ]);
//     }

//     #[Route('/{id}/edit', name: 'app_cart_edit', methods: ['GET', 'POST'])]
//     public function edit(Request $request, Cart $cart, EntityManagerInterface $entityManager): Response
//     {
//         $form = $this->createForm(CartType::class, $cart);
//         $form->handleRequest($request);

//         if ($form->isSubmitted() && $form->isValid()) {
//             $entityManager->flush();

//             return $this->redirectToRoute('app_cart_index', [], Response::HTTP_SEE_OTHER);
//         }

//         return $this->renderForm('cart/edit.html.twig', [
//             'cart' => $cart,
//             'form' => $form,
//         ]);
//     }

//     #[Route('/{id}', name: 'app_cart_delete', methods: ['POST'])]
//     public function delete(Request $request, Cart $cart, EntityManagerInterface $entityManager): Response
//     {
//         if ($this->isCsrfTokenValid('delete'.$cart->getId(), $request->request->get('_token'))) {
//             $entityManager->remove($cart);
//             $entityManager->flush();
//         }

//         return $this->redirectToRoute('app_cart_index', [], Response::HTTP_SEE_OTHER);
//     }
// }


namespace App\Controller;

use App\Entity\Product;
use App\Entity\Cart;
use App\Form\CartType;
use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


#[Route('/cart', name: 'cart_')]
class CartController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(SessionInterface $session, ProductRepository $productsRepository)
    {
        $cart = $session->get('cart', []);

        // On initialise des variables
        $data = [];
        $total = 0;

        foreach ($cart as $id => $quantity) {
            $product = $productsRepository->find($id);

            $data[] = [
                'product' => $product,
                'quantity' => $quantity
            ];
            $total += $product->getPrice() * $quantity;
        }

        return $this->render('cart/index.html.twig', compact('data', 'total'));
    }


    #[Route('/add/{id}', name: 'add')]
    public function add(Product $product, SessionInterface $session)
    {
        //On récupère l'id du produit
        $id = $product->getId();

        // On récupère le cart existant
        $cart = $session->get('cart', []);

        // On ajoute le produit dans le cart s'il n'y est pas encore
        // Sinon on incrémente sa quantité
        if (empty($cart[$id])) {
            $cart[$id] = 1;
        } else {
            $cart[$id]++;
        }

        $session->set('cart', $cart);

        //On redirige vers la page du cart
        return $this->redirectToRoute('cart_index');
    }

    #[Route('/remove/{id}', name: 'remove')]
    public function remove(Product $product, SessionInterface $session)
    {
        //On récupère l'id du produit
        $id = $product->getId();

        // On récupère le cart existant
        $cart = $session->get('cart', []);

        // On retire le produit du cart s'il n'y a qu'1 exemplaire
        // Sinon on décrémente sa quantité
        if (!empty($cart[$id])) {
            if ($cart[$id] > 1) {
                $cart[$id]--;
            } else {
                unset($cart[$id]);
            }
        }

        $session->set('cart', $cart);

        //On redirige vers la page du cart
        return $this->redirectToRoute('cart_index');
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Product $product, SessionInterface $session)
    {
        //On récupère l'id du produit
        $id = $product->getId();

        // On récupère le cart existant
        $cart = $session->get('cart', []);

        if (!empty($cart[$id])) {
            unset($cart[$id]);
        }

        $session->set('cart', $cart);

        //On redirige vers la page du cart
        return $this->redirectToRoute('cart_index');
    }

    #[Route('/empty', name: 'empty')]
    public function empty(SessionInterface $session)
    {
        $session->remove('cart');

        return $this->redirectToRoute('cart_index');
    }
}