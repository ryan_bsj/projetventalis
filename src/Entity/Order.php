<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $datetime_order = null;

    #[ORM\Column(length: 255)]
    private ?string $status_order = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Cart $cart = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateTimeOrder(): ?\DateTimeInterface
    {
        return $this->datetime_order;
    }

    public function setDateTimeOrder(\DateTimeInterface $datetime_order): static
    {
        $this->datetime_order = $datetime_order;

        return $this;
    }

    public function getStatusOrder(): ?string
    {
        return $this->status_order;
    }

    public function setStatusOrder(string $status_order): static
    {
        $this->status_order = $status_order;

        return $this;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): static
    {
        $this->cart = $cart;

        return $this;
    }
}
