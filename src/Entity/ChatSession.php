<?php

namespace App\Entity;

use App\Repository\ChatSessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChatSessionRepository::class)]
class ChatSession
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $subject = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $datetime_chat_session = null;

    #[ORM\Column]
    private ?bool $status_chat_session = null;

    #[ORM\OneToMany(mappedBy: 'chatSession', targetEntity: Message::class)]
    private Collection $message;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'chatSessions')]
    private Collection $user;

    public function __construct()
    {
        $this->message = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): static
    {
        $this->subject = $subject;

        return $this;
    }

    public function getDatetimeChatSession(): ?\DateTimeInterface
    {
        return $this->datetime_chat_session;
    }

    public function setDatetimeChatSession(\DateTimeInterface $datetime_chat_session): static
    {
        $this->datetime_chat_session = $datetime_chat_session;

        return $this;
    }

    public function isStatusChatSession(): ?bool
    {
        return $this->status_chat_session;
    }

    public function setStatusChatSession(bool $status_chat_session): static
    {
        $this->status_chat_session = $status_chat_session;

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessage(): Collection
    {
        return $this->message;
    }

    public function addMessage(Message $message): static
    {
        if (!$this->message->contains($message)) {
            $this->message->add($message);
            $message->setChatSession($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): static
    {
        if ($this->message->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getChatSession() === $this) {
                $message->setChatSession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): static
    {
        if (!$this->user->contains($user)) {
            $this->user->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): static
    {
        $this->user->removeElement($user);

        return $this;
    }
}
