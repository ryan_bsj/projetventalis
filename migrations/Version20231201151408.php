<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231201151408 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product_favorite (product_id INT NOT NULL, favorite_id INT NOT NULL, INDEX IDX_A375E44E4584665A (product_id), INDEX IDX_A375E44EAA17481D (favorite_id), PRIMARY KEY(product_id, favorite_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_favorite ADD CONSTRAINT FK_A375E44E4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_favorite ADD CONSTRAINT FK_A375E44EAA17481D FOREIGN KEY (favorite_id) REFERENCES favorite (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_favorite DROP FOREIGN KEY FK_A375E44E4584665A');
        $this->addSql('ALTER TABLE product_favorite DROP FOREIGN KEY FK_A375E44EAA17481D');
        $this->addSql('DROP TABLE product_favorite');
    }
}
